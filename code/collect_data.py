#!/usr/bin/env python3
"""Collect AMSR-E data for later processing.

The AMSR-E files are L2A data in HDF-EOS2 format. All files are read
and data that falls within the Antarctic region are saved to an output
file for later processing.

PROJ.4 is used to map each lat/lon to meters using a Lambert azimuthal
equal-area projection. This is used for an NSIDC EASE-2 grid, where
the grid size is 12.5 km.

"""
# Version history:
# v0.1 2016-07-16:
#    Created
# v0.2 2016-07-23:
#    Include incidence angle

import argparse
import sys
from pathlib import Path
import time
from datetime import datetime
import subprocess
import atexit
import typing

import numpy as np
import netCDF4
from netCDF4 import Dataset
from dateutil import tz
import pyproj


__author__ = "Richard Lindsley"
__version__ = "0.2"


def main() -> None:
    """Main function."""
    # ----------------
    # Parse arguments
    # ----------------
    parser = argparse.ArgumentParser(
        description='Collect AMSR-E data for azimuth modulation processing')
    parser.add_argument('list_fname', action='store',
                        help='List of AMSR-E L2A HDF-EOS2 files')
    parser.add_argument('out_fname', action='store',
                        help='Output file containing results (netCDF)')

    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(__version__))
    args = parser.parse_args()

    # Register exit function
    atexit.register(display_proc_time, time.perf_counter())

    # Create Proj object for the EASE-2 grid
    proj_ease = prepare_projection()

    # -----------------
    # Read in the list of files
    # -----------------
    print("Opening list file: {}".format(args.list_fname))
    with open(args.list_fname, "rt") as f:
        in_files = [line.rstrip() for line in f]

    print("{} files to read".format(len(in_files)))

    # -----------------
    # Prepare output file
    # -----------------
    print("Opening output file: {}".format(args.out_fname))
    with Dataset(args.out_fname, "w") as out_f:
        write_header(out_f)

        # -----------------
        # Loop over all input files and append to output
        # -----------------
        for file_i, in_fname in enumerate(in_files, start=1):
            print(("Processing file {}/{} "
                   "({:0.1%})").format(file_i, len(in_files),
                                       file_i / len(in_files)))

            process_file(in_fname, out_f, proj_ease)

    return


def write_header(out_f: Dataset) -> None:
    """Create and prepare the output file."""
    out_f.title = "AMSR-E data for Antarctica"
    out_f.Conventions = "CF-1.6,ACDD-1.3"
    out_f.creator_name = "Richard Lindsley"
    out_f.creator_type = "person"
    out_f.netcdf_version_id = netCDF4.getlibversion().split()[0]
    timestamp = datetime.strftime(datetime.now(tz.tzlocal()),
                                  "%Y-%m-%d %H:%M:%S%z")
    arg_list = [Path(__file__).name, ]
    arg_list.extend(sys.argv[1:])
    hist_entry = ("{} created by {} ({}) "
                  "as: {}").format(timestamp, arg_list[0],
                                   get_git_rev(),
                                   " ".join(arg_list))
    out_f.date_created = timestamp
    out_f.history = hist_entry

    out_f.region = 'antarctica'
    out_f.geospatial_lat_min = np.float32(-90)
    out_f.geospatial_lat_max = np.float32(-45)
    out_f.geospatial_lon_min = np.float32(-180)
    out_f.geospatial_lon_max = np.float32(180)

    # Create dimension and variables
    out_f.createDimension("record", None)

    out_f.createVariable("record", np.int64, ("record", ))

    filters = {'fletcher32': True, 'zlib': True}
    vars_f32 = ["azi", "inc", "lat", "lon", "x", "y",
                "tb_6h", "tb_6v", "tb_10h", "tb_10v",
                "tb_18h", "tb_18v", "tb_23h", "tb_23v",
                "tb_36h", "tb_36v"]
    vars_i16 = ["bin_row", "bin_col"]
    for v in vars_f32:
        out_f.createVariable(v, np.float32, ("record", ), **filters)
    for v in vars_i16:
        out_f.createVariable(v, np.int16, ("record", ), **filters)

    # Variable attributes
    out_f['record'].setncatts({
        'long_name': 'record index',
    })
    out_f['lat'].setncatts({
        'standard_name': 'latitude',
        'units': 'degrees_north',
        'valid_range': np.array([-90, 90], dtype=np.float32),
    })
    out_f['lon'].setncatts({
        'standard_name': 'longitude',
        'units': 'degrees_east',
        'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['bin_col'].setncatts({
        'long_name': 'horizontal index for output grid',
        'valid_range': np.array([0, 1440], dtype=np.int32),
    })
    out_f['bin_row'].setncatts({
        'long_name': 'vertical index for output grid',
        'valid_range': np.array([0, 1440], dtype=np.int32),
    })
    out_f['x'].setncatts({
        'long_name': 'x coordinate of projection',
        'standard_name': 'projection_x_coordinate',
        'units': 'm',
        'valid_range': np.array([-9e6, 9e6], dtype=np.float32),
    })
    out_f['y'].setncatts({
        'long_name': 'y coordinate of projection',
        'standard_name': 'projection_y_coordinate',
        'units': 'm',
        'valid_range': np.array([-9e6, 9e6], dtype=np.float32),
    })
    out_f['azi'].setncatts({
        'long_name': 'azimuth angle',
        'units': 'degree',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['inc'].setncatts({
        'long_name': 'incidence angle',
        'units': 'degree',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_6v'].setncatts({
        'long_name': 'AMSR-E Tb at 6.9 GHz, v-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_6h'].setncatts({
        'long_name': 'AMSR-E Tb at 6.9 GHz, h-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_10v'].setncatts({
        'long_name': 'AMSR-E Tb at 10.7 GHz, v-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_10h'].setncatts({
        'long_name': 'AMSR-E Tb at 10.7 GHz, h-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_18v'].setncatts({
        'long_name': 'AMSR-E Tb at 18.7 GHz, v-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_18h'].setncatts({
        'long_name': 'AMSR-E Tb at 18.7 GHz, h-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_23v'].setncatts({
        'long_name': 'AMSR-E Tb at 23.8 GHz, v-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_23h'].setncatts({
        'long_name': 'AMSR-E Tb at 23.8 GHz, h-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_36v'].setncatts({
        'long_name': 'AMSR-E Tb at 36.5 GHz, v-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['tb_36h'].setncatts({
        'long_name': 'AMSR-E Tb at 36.5 GHz, h-pol',
        'units': 'K',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })


def get_git_rev() -> str:
    """Return the git commit short hash for the CWD."""
    r = subprocess.run(['git', 'rev-parse', '--short', 'HEAD'],
                       stdout=subprocess.PIPE, check=True)

    result = typing.cast(bytes, r.stdout)
    return result.rstrip().decode()


def process_file(in_fname: str,
                 out_f: Dataset,
                 proj_ease: pyproj.Proj) -> None:
    """Read the AMSR-E L2A file and write out the relevant data.

    in_fname: the input netCDF file to read
    out_f: the output netCDF Dataset object
    proj_ease: the pyproj object to map the locations

    """
    with Dataset(in_fname, "r") as in_f:
        # Note that the scale_factor and add_offset attributes are
        # non-standard, so are ignored by netcdf4-python. We apply
        # them manually here.
        lat = in_f.variables['Low_Res_Swath_latitude'][:]
        lon = in_f.variables['Low_Res_Swath_longitude'][:]
        azi = in_f.variables['Earth_Azimuth'][:] * 0.01
        inc = in_f.variables['Earth_Incidence'][:] * 0.005

        tb_6v = in_f.variables['_6_9V_Res_1_TB__not_resampled_'][:]
        tb_6v = tb_6v * 0.01 + 327.68
        tb_6h = in_f.variables['_6_9H_Res_1_TB__not_resampled_'][:]
        tb_6h = tb_6h * 0.01 + 327.68
        tb_10v = in_f.variables['_10_7V_Res_2_TB__not_resampled_'][:]
        tb_10v = tb_10v * 0.01 + 327.68
        tb_10h = in_f.variables['_10_7H_Res_2_TB__not_resampled_'][:]
        tb_10h = tb_10h * 0.01 + 327.68
        tb_18v = in_f.variables['_18_7V_Res_3_TB__not_resampled_'][:]
        tb_18v = tb_18v * 0.01 + 327.68
        tb_18h = in_f.variables['_18_7H_Res_3_TB__not_resampled_'][:]
        tb_18h = tb_18h * 0.01 + 327.68
        tb_23v = in_f.variables['_23_8V_Approx__Res_3_TB__not_resampled_'][:]
        tb_23v = tb_23v * 0.01 + 327.68
        tb_23h = in_f.variables['_23_8H_Approx__Res_3_TB__not_resampled_'][:]
        tb_23h = tb_23h * 0.01 + 327.68
        tb_36v = in_f.variables['_36_5V_Res_4_TB__not_resampled_'][:]
        tb_36v = tb_36v * 0.01 + 327.68
        tb_36h = in_f.variables['_36_5H_Res_4_TB__not_resampled_'][:]
        tb_36h = tb_36h * 0.01 + 327.68

        # The quality for channels from 6 to 36 GHz
        lo_qual = in_f.variables['Channel_Quality_Flag_6_to_52'][:]

    # The region is simple: everything below -50 degrees North
    region_mask = lat < -50

    # We do a strict check: if any values (in any of the channels, so
    # elements 0..10 in dimension 1 of lo_qual) do not have a 0
    # quality flag, we exclude it
    qual_mask = np.all(lo_qual[:, 0:10] == 0, axis=1)

    # Infrequently, a "good" TB value is nevertheless 0 K, so we also
    # mask out these. I've found that if one channel has zero values,
    # the rest are set to zero as well. So I only check one channel.
    zero_mask = (tb_6h > 10.0)

    tot_mask = region_mask & qual_mask[:, np.newaxis] & zero_mask
    num_good = tot_mask.sum()
    num_total = region_mask.size
    print((" {}/{} ({:0.1%}) "
           "good measurements").format(num_good, num_total,
                                       num_good / num_total))

    # Map the location and bin it
    map_x, map_y = proj_ease(lon[tot_mask], lat[tot_mask], errcheck=True)
    cols, rows = bin_map(map_x, map_y)

    # Append to output file
    out_len = len(out_f['record'])
    out_inds = np.s_[out_len:out_len + num_good]
    out_f['record'][out_inds] = out_len + np.arange(num_good)
    out_f['lat'][out_inds] = lat[tot_mask]
    out_f['lon'][out_inds] = lon[tot_mask]
    out_f['x'][out_inds] = map_x
    out_f['y'][out_inds] = map_y
    out_f['bin_row'][out_inds] = rows
    out_f['bin_col'][out_inds] = cols
    out_f['azi'][out_inds] = azi[tot_mask]
    out_f['inc'][out_inds] = inc[tot_mask]
    out_f['tb_6v'][out_inds] = tb_6v[tot_mask]
    out_f['tb_6h'][out_inds] = tb_6h[tot_mask]
    out_f['tb_10v'][out_inds] = tb_10v[tot_mask]
    out_f['tb_10h'][out_inds] = tb_10h[tot_mask]
    out_f['tb_18v'][out_inds] = tb_18v[tot_mask]
    out_f['tb_18h'][out_inds] = tb_18h[tot_mask]
    out_f['tb_23v'][out_inds] = tb_23v[tot_mask]
    out_f['tb_23h'][out_inds] = tb_23h[tot_mask]
    out_f['tb_36v'][out_inds] = tb_36v[tot_mask]
    out_f['tb_36h'][out_inds] = tb_36h[tot_mask]


def bin_map(x: np.ndarray,
            y: np.ndarray) -> typing.Tuple[np.ndarray, np.ndarray]:
    """Bin the map coordinates into a 12.5 km EASE-2 grid.

    The 12.5 km EASE grid is 1440 x 1440 pixels and is centered at the
    South Pole.

    The x and y arrays contain the offset in meters from the origin
    (South Pole) along the map. So really this is just determining
    which bin index each x/y location belongs to.

    The returned bin indices range from 0 to 1440, exclusive. The
    south pole lies right between bins 719 and 720 in both dimensions
    (note 0-based indexing).

    So, for example, when x is between 0 and 12500, it belongs to bin
    720.

    """
    bin_edges = (np.arange(1441) - 720) * 12500.0

    cols = np.digitize(x, bin_edges)
    rows = np.digitize(y, bin_edges)

    # We should never be out of bounds, but let's make sure
    if np.any(cols == 0) or np.any(cols == 1441) or \
       np.any(rows == 0) or np.any(rows == 1441):
        raise Exception("Location out of bounds?!")

    # Because digitize returns the bin index for bins[i-1] <= x <
    # bins[i], we have to shift it down by one
    cols -= 1
    rows -= 1

    return (cols, rows)


def prepare_projection() -> pyproj.Proj:
    """Create a Proj object for projecting location to a map.

    PROJ.4 is used to convert from lat/lon to x/y coordinates on a map
    projection. The map projection is the EASE-2 South Hemisphere grid
    (Lambert azimuthal equal-area projection)

    https://nsidc.org/data/ease/ease_grid2.html

    https://epsg.io/6932

    """
    return pyproj.Proj(proj='laea', lat_0=-90, lon_0=0, x_0=0, y_0=0,
                       ellps='WGS84', datum='WGS84', units='m')


def display_proc_time(start_time: float) -> None:
    """Display how long the processing took.

    start_time: when time.perf_counter() was called at the start of processing

    """
    dt = time.perf_counter() - start_time
    print("Finished collection in {:0.2f} s".format(dt))


if __name__ == "__main__":
    main()
