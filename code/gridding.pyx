#cython: language_level=3, boundscheck=False, cdivision=True

cimport posix.time
from libc.math cimport sqrt, hypot, atan2, sin, cos, M_PI, M_1_PI
from libc.stdint cimport int32_t, int16_t
from libc.stdio cimport printf

import numpy as np
cimport numpy as np
# from scipy.linalg.cython_lapack cimport dsysvx
cimport cython
# from cython.parallel cimport prange

cdef extern from "lapacke.h":
    void dsysvx_(char* fact, char* uplo, int* n, int* nrhs,
                 const double* a, int* lda, double* af,
                 int* ldaf, int* ipiv, const double* b,
                 int* ldb, double* x, int* ldx, double* rcond,
                 double* ferr, double* berr, double* work, int* lwork,
                 int* iwork, int *info ) nogil

def process_data(dict meas_in, dict grid_out):
    """Scan through the AMSR-E measurements and grid the data."""
    # NB that the multi-dimensional array ordering used here makes a
    # *big* performance difference due to CPU cache effects. The
    # arrays are in row-major order so the last indices mean the data
    # is arranged consecutively.

    # Shortcut variables so I only do dict lookups once; also
    # concatenate Tb measurements
    # cdef unsigned int num_recs = meas_in['num_recs']
    cdef int16_t [:] bin_rows = meas_in['bin_rows']
    cdef int16_t [:] bin_cols = meas_in['bin_cols']
    cdef float [:] meas_azi = meas_in['azi']

    cdef unsigned int num_chan = 10
    cdef float [:, :] meas_tb = meas_in['tb']  # dims: (rec, channel)

    cdef int16_t [:, :] num_hits = grid_out['num_hits']
    cdef double [:, :] matrix_cond = grid_out['matrix_cond']
    cdef double [:, :] grid_azi_m_r = grid_out['azi_m1_r']
    cdef double [:, :] grid_azi_m_i = grid_out['azi_m1_i']
    cdef double [:, :, :] grid_mean = grid_out['mean']
    cdef double [:, :, :] grid_mag0 = grid_out['mag_0']
    cdef double [:, :, :] grid_mag1 = grid_out['mag_1']
    cdef double [:, :, :] grid_mag2 = grid_out['mag_2']
    cdef double [:, :, :] grid_ang1 = grid_out['ang_1']
    cdef double [:, :, :] grid_ang2 = grid_out['ang_2']
    cdef double [:, :, :] grid_mean_err_mean = grid_out['mean_err_mean']
    cdef double [:, :, :] grid_mean_err_std = grid_out['mean_err_std']
    cdef double [:, :, :] grid_azi_err_mean = grid_out['azi_err_mean']
    cdef double [:, :, :] grid_azi_err_std = grid_out['azi_err_std']

    # For the least-squares estimation, I need to solve the equation
    # Ax = b (for each channel separately). However, it's inefficient
    # to keep looping back and forth across records and pixels to
    # build up the required arrays, so I do an online algorithm. I
    # instead compute and store the quantities A'A and A'b (where ' is
    # transpose). The number of rows in the arrays A and b vary across
    # the output grid. However, the matrix shape is constant for A'A
    # (5, 5) and A'b (5, 1).

    # Further note that since I'm computing 10 channels, I actually
    # store the 10 column vectors A'b into a matrix A'B. This is just
    # column-stacking A'b for each channel.
    #
    # Since the arrays are constant size (5, 5) and (5, 10) for each
    # grid output, I construct working arrays for the entire output
    # grid. They are ordered as (grid_col, grid_row, 5, 5) and
    # (grid_col, grid_row, 5, 10). (In principle I can store just the
    # upper diagonal of A'A since it's Hermitian, but the memory
    # savings isn't that dramatic, since I can't do anything to
    # compress the matrix A'b.)

    cdef int num_coef = 5
    cdef double [:, :, :, :] grid_ata = np.zeros((num_hits.shape[0], num_hits.shape[1], num_coef, num_coef))
    cdef double [:, :, :, :] grid_atb = np.zeros((num_hits.shape[0], num_hits.shape[1], num_coef, num_chan))

    # ----------------
    # Phase one: compute the mean and number of hits per pixel,
    # prepare matrices for least squares
    printf("  gridding data\n")
    prepare_grid(bin_rows, bin_cols, meas_tb, meas_azi, grid_mean, num_hits, grid_azi_m_r, grid_azi_m_i, grid_ata, grid_atb)

    # ----------------
    # Phase two: estimate the model parameters
    printf("  estimating model coefficients\n")
    estimate_model(num_hits, grid_ata, grid_atb, matrix_cond,
                   grid_mag0, grid_mag1, grid_mag2, grid_ang1, grid_ang2)

    # ----------------
    # Phase three: find the model residual (difference between
    # measurements and forward model)
    cdef double [:] mean_pix_err = np.zeros(num_chan, dtype=np.float64)
    cdef double [:] azi_pix_err = np.zeros(num_chan, dtype=np.float64)
    num_hits[:, :] = 0
    printf("  computing error\n")
    find_grid_error(bin_rows, bin_cols, meas_tb, meas_azi,
                    grid_mean, grid_mag0, grid_mag1, grid_mag2,
                    grid_ang1, grid_ang2, num_hits,
                    mean_pix_err, azi_pix_err,
                    grid_mean_err_mean, grid_mean_err_std,
                    grid_azi_err_mean, grid_azi_err_std)


cdef void prepare_grid(int16_t [:] bin_rows, int16_t [:] bin_cols,
                       float [:, :] meas_tb, float [:] meas_azi,
                       double [:, :, :] grid_mean,
                       int16_t [:, :] num_hits,
                       double [:, :] grid_azi_m_r, double [:, :] grid_azi_m_i,
                       double [:, :, :, :] grid_ata, double [:, :, :, :] grid_atb) nogil:
    """First pass of gridding.

    Compute the mean Tb per pixel and the number of hits. Also prepare
    matrices for least squares.
    """
    cdef unsigned int num_recs = meas_tb.shape[0]
    cdef unsigned int num_chan = grid_mean.shape[2]
    cdef unsigned int num_coef = grid_ata.shape[2]

    cdef unsigned int rec_i, n
    cdef unsigned int bin_row, bin_col, chan_i, coef_i
    cdef double coefs[5]
    cdef unsigned int a_col, a_row

    if num_coef != 5:
        printf("ERROR: num_coef != 5\n")

    last_display = time_tick()
    for rec_i in range(num_recs):
        if (rec_i + 1) % 1000 == 0:
            now = time_tick()
            if now - last_display > 2:
                printf("  meas %d/%d (%0.1f%%)\n", rec_i + 1, num_recs, (rec_i + 1) * 100.0 / num_recs)
                last_display = now

        bin_row = bin_rows[rec_i]
        bin_col = bin_cols[rec_i]

        num_hits[bin_col, bin_row] += 1

        # An online algorithm to update the mean
        n = num_hits[bin_col, bin_row]

        for chan_i in range(num_chan):
            grid_mean[bin_col, bin_row, chan_i] += (meas_tb[rec_i, chan_i] - grid_mean[bin_col, bin_row, chan_i]) / n

        coefs[0] = 1
        coefs[1] = cos(deg2rad(meas_azi[rec_i]))
        coefs[2] = sin(deg2rad(meas_azi[rec_i]))
        coefs[3] = cos(2 * deg2rad(meas_azi[rec_i]))
        coefs[4] = sin(2 * deg2rad(meas_azi[rec_i]))

        # Since azimuth angle is a circular quantity, its computation
        # is different. See
        # https://en.wikipedia.org/wiki/Directional_statistics. I want
        # to compute sum_i=1..N e^(j theta_i), but it's modified to
        # use an online algorithm. Note that the complex number is
        # stored in real/imag components.

        grid_azi_m_r[bin_col, bin_row] += (coefs[1] - grid_azi_m_r[bin_col, bin_row]) / n
        grid_azi_m_i[bin_col, bin_row] += (coefs[2] - grid_azi_m_i[bin_col, bin_row]) / n

        # Now update the matrix A'A. This is just adding the outer
        # product coefs'coefs to A'A.
        for a_row in range(5):
            for a_col in range(5):
                grid_ata[bin_col, bin_row, a_row, a_col] += coefs[a_row] * coefs[a_col]

        # Update the column vectors A'b for each channel
        for coef_i in range(5):
            for chan_i in range(num_chan):
                grid_atb[bin_col, bin_row, coef_i, chan_i] += meas_tb[rec_i, chan_i] * coefs[coef_i]


cdef void estimate_model(int16_t [:, :] num_hits,
                         double [:, :, :, :] grid_ata, double [:, :, :, :] grid_atb,
                         double [:, :] matrix_cond, double [:, :, :] grid_mag0,
                         double [:, :, :] grid_mag1, double [:, :, :] grid_mag2,
                         double [:, :, :] grid_ang1, double [:, :, :] grid_ang2) nogil:
    """Estimate the model coefficients from the pre-computed matrices."""
    cdef unsigned int cur_pixel = 0
    cdef unsigned int tot_pixels = num_hits.shape[0] * num_hits.shape[1]
    cdef int col_i, row_i, chan_i
    cdef int num_coef = 5
    cdef int num_chan = 10
    cdef int min_hits = 10

    cdef unsigned int i, j

    cdef double a[5*5]  # column-major order, (rows, cols) = (5, 5)
    cdef double b[10*5]  # column-major order, (rows, cols) = (5, 10)
    cdef int ipiv[5]
    cdef double work[5*64]
    cdef int lwork = 5 * 64
    cdef int info = 0
    cdef int lda = 5  # max(1, N); N = 5
    cdef int ldb = 5  # max(1, N); N = 5
    cdef int ldaf = 5  # max(1, N); N = 5
    cdef double af[5*5]  # I don't use this, but it's required for dsysvx
    cdef double x[10*5]
    cdef int ldx = 5
    cdef double rcond = 0
    cdef double ferr[10]
    cdef double berr[10]
    cdef int iwork[5]

    last_display = time_tick()
    for col_i in range(num_hits.shape[0]):
        for row_i in range(num_hits.shape[1]):
            cur_pixel += 1
            if cur_pixel % 1000 == 0:
                now = time_tick()
                if now - last_display > 2:
                    printf("  pixel %d/%d (%0.1f%%)\n", cur_pixel, tot_pixels, cur_pixel * 100.0 / tot_pixels)
                    last_display = now

            # Skip pixels with insufficient hits
            if num_hits[col_i, row_i] < min_hits:
                continue

            # Solve for the coefficients using LAPACK. Since I have
            # A'A, I don't need a 'general matrix' driver, but I use a
            # symmetric matrix (since (A'A)' = A'A) driver. I choose
            # the DSYSVX routine. This uses an LU decomposition to
            # solve the matrix equation (A'A)x = A'b, where A'A and
            # A'b were built up in prepare_grid(). A is a sin/cos
            # matrix, x is the coefficient vector for a single
            # channel, and b is the vector of brightness temperatures
            # for a single channel. The process is done for multiple
            # channels at once by stacking the column vectors of b
            # into the matrix B. Then the output matrix X contains
            # column vectors of (output) coefficients.

            # I don't want to overwrite the input matrix a (A'A), so I
            # make a local copy. Note that LAPACK expects the matrices
            # in Fortran (column-major) order, but since A'A is
            # symmetric, this doesn't matter. But the other
            # input/output matrices b (A'b, input only) and x (x,
            # output only) are not symmetric, so therefore column-major
            # ordering matters.

            for i in range(5):
                for j in range(5):
                    a[i + 5*j] = grid_ata[col_i, row_i, i, j]

            for i in range(5):
                for j in range(10):
                    b[i + 5*j] = grid_atb[col_i, row_i, i, j]

            dsysvx_('N', 'U', &num_coef, &num_chan, a, &lda,
                    af, &ldaf, ipiv, b, &ldb, x, &ldx, &rcond,
                    ferr, berr, work, &lwork, iwork, &info)

            if info != 0:
                printf("ERROR: pix: (%d, %d) info: %d\n", col_i, row_i, info)

            # Store the pixel results
            matrix_cond[col_i, row_i] = 1 / rcond
            for chan_i in range(num_chan):
                grid_mag0[col_i, row_i, chan_i] = x[0 + 5*chan_i]
                grid_mag1[col_i, row_i, chan_i] = hypot(x[1 + 5*chan_i], x[2 + 5*chan_i])
                grid_mag2[col_i, row_i, chan_i] = hypot(x[3 + 5*chan_i], x[4 + 5*chan_i])
                grid_ang1[col_i, row_i, chan_i] = rad2deg(atan2(x[2 + 5*chan_i], x[1 + 5*chan_i]))
                grid_ang2[col_i, row_i, chan_i] = rad2deg(atan2(x[4 + 5*chan_i], x[3 + 5*chan_i]))


cdef void find_grid_error(int16_t [:] bin_rows, int16_t [:] bin_cols,
                          float [:, :] meas_tb, float [:] meas_azi,
                          double [:, :, :] grid_mean, double [:, :, :] grid_mag0,
                          double [:, :, :] grid_mag1, double [:, :, :] grid_mag2,
                          double [:, :, :] grid_ang1, double [:, :, :] grid_ang2,
                          int16_t [:, :] num_hits,
                          double [:] mean_pix_err, double [:] azi_pix_err,
                          double [:, :, :] grid_mean_err_mean,
                          double [:, :, :] grid_mean_err_std,
                          double [:, :, :] grid_azi_err_mean,
                          double [:, :, :] grid_azi_err_std) nogil:
    """Find the residual error for the entire output grid."""
    cdef int num_recs = meas_tb.shape[0]
    cdef int num_chan = mean_pix_err.shape[0]

    cdef int rec_i, bin_col, bin_row, chan_i

    last_display = time_tick()
    for rec_i in range(num_recs):
        if (rec_i + 1) % 1000 == 0:
            now = time_tick()
            if now - last_display > 2:
                printf("  meas %d/%d (%0.1f%%)\n", rec_i + 1, num_recs, (rec_i + 1) * 100.0 / num_recs)
                last_display = now

        # Compute the forward model values (for each channel) for this
        # measurement and difference it against the measured Tb
        # values. It's separately done for the "mean" and "azi" models.
        find_mean_meas_error(rec_i, bin_rows, bin_cols, meas_tb, grid_mean, mean_pix_err)
        find_azi_meas_error(rec_i, bin_rows, bin_cols, meas_tb, meas_azi,
                            grid_mag0, grid_mag1, grid_mag2, grid_ang1, grid_ang2,
                            azi_pix_err)

        # Online algorithm to update the mean and std error
        update_grid_errors(bin_rows[rec_i], bin_cols[rec_i], num_hits,
                           mean_pix_err, grid_mean_err_mean, grid_mean_err_std,
                           azi_pix_err, grid_azi_err_mean, grid_azi_err_std)

    # Normalize grid_err_std from M2 into variance, and then into standard deviation
    for bin_col in range(num_hits.shape[0]):
        for bin_row in range(num_hits.shape[1]):
            for chan_i in range(num_chan):
                grid_mean_err_std[bin_col, bin_row, chan_i] /= num_hits[bin_col, bin_row]
                grid_mean_err_std[bin_col, bin_row, chan_i] = sqrt(grid_mean_err_std[bin_col, bin_row, chan_i])
                grid_azi_err_std[bin_col, bin_row, chan_i] /= num_hits[bin_col, bin_row]
                grid_azi_err_std[bin_col, bin_row, chan_i] = sqrt(grid_azi_err_std[bin_col, bin_row, chan_i])


cdef void find_mean_meas_error(unsigned int rec_i,
                               int16_t [:] bin_rows,
                               int16_t [:] bin_cols,
                               float [:, :] meas_tb,
                               double [:, :, :] grid_mean,
                               double [:] pix_err) nogil:
    """Compute the residual error for all channels for a single measurement.

    This version uses the "mean" model: the predicted value is simply
    the mean of all brightness temperatures.

    """
    cdef unsigned int bin_row = bin_rows[rec_i]
    cdef unsigned int bin_col = bin_cols[rec_i]

    cdef unsigned int num_chan = pix_err.shape[0]

    for chan_i in range(num_chan):
        pix_err[chan_i] = meas_tb[rec_i, chan_i] - grid_mean[bin_col, bin_row, chan_i]


cdef void find_azi_meas_error(unsigned int rec_i,
                              int16_t [:] bin_rows,
                              int16_t [:] bin_cols,
                              float [:, :] meas_tb,
                              float [:] meas_azi,
                              double [:, :, :] grid_mag0,
                              double [:, :, :] grid_mag1,
                              double [:, :, :] grid_mag2,
                              double [:, :, :] grid_ang1,
                              double [:, :, :] grid_ang2,
                              double [:] pix_err) nogil:
    """Compute the residual error for all channels for a single measurement.

    This version uses the "azi" model: the predicted value is a
    Fourier series using the previously estimated model coefficients.

    """
    cdef unsigned int bin_row = bin_rows[rec_i]
    cdef unsigned int bin_col = bin_cols[rec_i]

    cdef unsigned int num_chan = pix_err.shape[0]
    cdef double predicted_tb, b0, b1, b2, c1, c2

    for chan_i in range(num_chan):
        # Convert the Fourier series coefficients from polar
        # coordinates back to rectangular
        b0 = grid_mag0[bin_col, bin_row, chan_i]
        b1 = grid_mag1[bin_col, bin_row, chan_i] * cos(deg2rad(grid_ang1[bin_col, bin_row, chan_i]))
        c1 = grid_mag1[bin_col, bin_row, chan_i] * sin(deg2rad(grid_ang1[bin_col, bin_row, chan_i]))
        b2 = grid_mag2[bin_col, bin_row, chan_i] * cos(deg2rad(grid_ang2[bin_col, bin_row, chan_i]))
        c2 = grid_mag2[bin_col, bin_row, chan_i] * sin(deg2rad(grid_ang2[bin_col, bin_row, chan_i]))

        # Apply the Fourier series fit
        predicted_tb = b0 + b1 * cos(deg2rad(meas_azi[rec_i])) + \
                       c1 * sin(deg2rad(meas_azi[rec_i])) + \
                       b2 * cos(2 * deg2rad(meas_azi[rec_i])) + \
                       c2 * sin(2 * deg2rad(meas_azi[rec_i]))

        # And here's our error
        pix_err[chan_i] = meas_tb[rec_i, chan_i] - predicted_tb


cdef void update_grid_errors(unsigned int bin_row, unsigned int bin_col,
                             int16_t [:, :] num_hits,
                             double [:] mean_pix_err,
                             double [:, :, :] grid_mean_err_mean,
                             double [:, :, :] grid_mean_err_std,
                             double [:] azi_pix_err,
                             double [:, :, :] grid_azi_err_mean,
                             double [:, :, :] grid_azi_err_std) nogil:
    """Update the residual error mean/std arrays.

    An online algorithm is used to find the variance. Note here the
    pix_err_std is used as M2. When finished (after this function
    iterates over all pixels), normalize it to get the variance/std.

    The error metrics (mean/std) are updated for both models
    (mean/azi) together.

    """
    cdef unsigned int num_chan = mean_pix_err.shape[0]
    cdef double delta

    # Note that grid_*_err_std acts as M2 until the loop is done.
    num_hits[bin_col, bin_row] += 1
    cdef unsigned int n = num_hits[bin_col, bin_row]

    for chan_i in range(num_chan):
        delta = mean_pix_err[chan_i] - grid_mean_err_mean[bin_col, bin_row, chan_i]
        grid_mean_err_mean[bin_col, bin_row, chan_i] += delta / n
        grid_mean_err_std[bin_col, bin_row, chan_i] += delta * (mean_pix_err[chan_i] - grid_mean_err_mean[bin_col, bin_row, chan_i])

        delta = azi_pix_err[chan_i] - grid_azi_err_mean[bin_col, bin_row, chan_i]
        grid_azi_err_mean[bin_col, bin_row, chan_i] += delta / n
        grid_azi_err_std[bin_col, bin_row, chan_i] += delta * (azi_pix_err[chan_i] - grid_azi_err_mean[bin_col, bin_row, chan_i])


cpdef list extract_data(dict meas_in, unsigned int col, unsigned int row):
    """Scan through the AMSR-E data and return the indices.

    A Python list is created and appended to for every measurement
    that falls within the specified bin index. The list contains the
    measurement indices for the bin.

    """
    # Shortcut variables so I only do dict lookups once
    cdef unsigned int num_recs = meas_in['num_recs']
    cdef np.ndarray[np.int16_t, ndim=1] bin_rows = meas_in['bin_rows']
    cdef np.ndarray[np.int16_t, ndim=1] bin_cols = meas_in['bin_cols']

    cdef unsigned int rec_i
    cdef unsigned int bin_row, bin_col

    cdef list inds = []

    cdef double last_display, now

    last_display = time_tick()
    print("  scanning data")
    for rec_i in range(num_recs):
        if (rec_i + 1) % 1000 == 0:
            now = time_tick()
            if now - last_display > 2:
                print("  meas {}/{} ({:0.1%})".format(rec_i + 1, num_recs,
                                                      (rec_i + 1) * 1.0 / num_recs))
                last_display = now

        bin_row = bin_rows[rec_i]
        bin_col = bin_cols[rec_i]

        if bin_row == row and bin_col == col:
            inds.append(rec_i)

    return inds


cdef double time_tick() nogil:
    """Use the monotonic clock to get a time tick.

    This is equivalent to (but not guaranteed to return the same
    values as) time.perf_counter() in the Python standard library.

    """
    cdef posix.time.timespec tick
    posix.time.clock_gettime(posix.time.CLOCK_MONOTONIC_COARSE, &tick)

    return tick.tv_nsec / 1.0e9 + tick.tv_sec


cdef double deg2rad(double x) nogil:
    """Convert quantity from degrees to radians."""
    return x * M_PI / 180.0


cdef double rad2deg(double x) nogil:
    """Convert quantity from radians to degrees."""
    return x * 180.0 * M_1_PI
