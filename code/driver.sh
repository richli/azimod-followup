#!/usr/bin/env bash
set -euo pipefail

out_dir="/mnt/cargo/mers/azimod-followup/data/"

# Download AMSR-E L2A files
# ./download_sources.sh

# Convert from HDF-EOS2 to netCDF4
# find "$out_dir" -name '*.hdf' -execdir h4tonccf '{}' \;

# Collect the data and prepare for gridding
# python ./collect_data.py <(find "$out_dir" -name '*.nc') amsr_Ant_hits.nc

# (DEPRECATED)
# # Index the data for faster processing
# # rm -f amsr_Ant_index.db
# # python ./index_data.py amsr_Ant_hits.nc amsr_Ant_index.db

# Ensure the Cython module is up-to-date
# make

# Grid the data and fit it to the model
# python ./grid_data.py amsr_Ant_hits.nc amsr_Ant_grid.nc

# Make plots for a certain pixel:
# 126.490 E -67.929 N
# row=603
# col=877
# python ./plot_azi_pixel.py amsr_Ant_hits.nc "$col" "$row"
# python ./plot_azi_pixel.py amsr_Ant_hits.nc "$col" "$row" --dx 1 --dy 1

# Plot the grid results
python ./plot_azi_grids.py amsr_Ant_grid.nc --out-dir=./ --plt-fmt=png
