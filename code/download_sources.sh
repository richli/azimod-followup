#!/usr/bin/env bash

# Download AMSR-E L2A files from the NSIDC

out_dir="/mnt/cargo/mers/azimod-followup/data/"
src_dir="ftp://n5eil01u.ecs.nsidc.org/SAN/AMSA/AE_L2A.003/"

mkdir -p "$out_dir"

dates=(
	"2009.07.30"
	"2009.07.31"
	"2009.08.01"
	"2009.08.02"
	"2009.08.03"
	"2009.08.04"
	"2009.08.05"
	"2009.08.06"
	"2009.08.07"
	"2009.08.08"
	"2009.08.09"
	"2009.08.10"
	"2009.08.11"
	"2009.08.12"
	"2009.08.13"
	"2009.08.14"
	"2009.08.15"
	"2009.08.16"
	"2009.08.17"
	"2009.08.18"
	"2009.08.19"
	"2009.08.20"
	"2009.08.21"
	"2009.08.22"
	"2009.08.23"
	"2009.08.24"
	"2009.08.25"
	"2009.08.26"
	"2009.08.27"
	"2009.08.28"
)

for date in "${dates[@]}"; do
	lftp -c mirror --include-glob='*.hdf' --continue --parallel=2 "$src_dir/$date" "$out_dir"
done
