#!/usr/bin/env python3
"""Plot AMSR-E Tb vs azimuth angle for a specified grid cell.

The input data comes from collect_data.py. A particular row/col is
requested and the data is filtered to select all measurements for that
grid cell. Then scatter plots of the AMSR-E brightness temperatures
versus azimuth angle are created.

"""
# Version history:
# v0.1 2016-07-21:
#    Created

import argparse
import time
import atexit
import typing
import json

import numpy as np
# from numpy import ma
from netCDF4 import Dataset
import pyproj
import matplotlib.pyplot as plt

from gridding import extract_data

__author__ = "Richard Lindsley"
__version__ = "0.1"


def main() -> None:
    """Main function."""
    # ----------------
    # Parse arguments
    # ----------------
    parser = argparse.ArgumentParser(
        description='Plot AMSR-E data for a single grid cell')
    parser.add_argument('in_fname', action='store',
                        help='AMSR-E data in the Antarctic region (netCDF)')
    parser.add_argument('col', action='store', type=int,
                        help='Col number (x coordinate)')
    parser.add_argument('row', action='store', type=int,
                        help='Row number (y coordinate)')

    parser.add_argument('--dx', action='store', type=int, default=0,
                        help='Include this many neighbors in x direction')
    parser.add_argument('--dy', action='store', type=int, default=0,
                        help='Include this many neighbors in y direction')

    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(__version__))
    args = parser.parse_args()

    # Register exit function
    atexit.register(display_proc_time, time.perf_counter())

    print(("Requested data for "
           "(x={}, y={}; 0-based indexing)").format(args.col, args.row))
    if args.col >= 1440 or args.col < 0:
        parser.error("Column must be in [0, 1440)")
    if args.row >= 1440 or args.row < 0:
        parser.error("Row must be in [0, 1440)")
    if args.dx < 0:
        parser.error("dx must be positive")
    if args.dy < 0:
        parser.error("dy must be positive")

    num_cols = num_rows = 1440
    x_vals = (np.arange(num_cols) - num_cols // 2) * 12500.0 + (12500.0 / 2)
    y_vals = (np.arange(num_rows) - num_rows // 2) * 12500.0 + (12500.0 / 2)
    bin_x = x_vals[args.col]
    bin_y = y_vals[args.row]
    print("Bin center in map coordinates: ({} km, {} km)".format(bin_x, bin_y))

    proj_ease = pyproj.Proj(proj='laea', lat_0=-90, lon_0=0, x_0=0, y_0=0,
                            ellps='WGS84', datum='WGS84', units='m')

    bin_lon, bin_lat = proj_ease(bin_x, bin_y, inverse=True, errcheck=True)
    print(("Bin center in lat/lon: "
           "({:0.2f} °N, {:0.2f} °E)").format(bin_lat, bin_lon))

    if args.dx > 0:
        print((" also including {} neighboring cells "
               "in x direction").format(args.dx))
    if args.dy > 0:
        print((" also including {} neighboring cells "
               "in y direction").format(args.dy))

    # -----------------
    # Load input
    # -----------------
    print("Opening input file: {}".format(args.in_fname))
    with Dataset(args.in_fname, "r") as in_f:
        print("  loading data")
        meas_in = {
            'num_recs': len(in_f.dimensions['record']),
            'bin_rows': in_f.variables['bin_row'][:],
            'bin_cols': in_f.variables['bin_col'][:],
            'tb_6h': in_f.variables['tb_6h'][:],
            'tb_6v': in_f.variables['tb_6v'][:],
            'tb_10h': in_f.variables['tb_10h'][:],
            'tb_10v': in_f.variables['tb_10v'][:],
            'tb_18h': in_f.variables['tb_18h'][:],
            'tb_18v': in_f.variables['tb_18v'][:],
            'tb_23h': in_f.variables['tb_23h'][:],
            'tb_23v': in_f.variables['tb_23v'][:],
            'tb_36h': in_f.variables['tb_36h'][:],
            'tb_36v': in_f.variables['tb_36v'][:],
            'azi': in_f.variables['azi'][:],
            'inc': in_f.variables['inc'][:],
        }

    # -----------------
    # Extract measurements for the grid point(s)
    # -----------------
    inds = []
    for dx in range(-args.dx, args.dx + 1):
        for dy in range(-args.dy, args.dy + 1):
            x = args.col + dx
            y = args.row + dy
            print("Extracting indices for ({}, {})".format(x, y))
            cell_inds = extract_data(meas_in, x, y)
            print("  {} measurements present".format(len(cell_inds)))
            inds.extend(cell_inds)

    # -----------------
    # Plot results
    # -----------------
    azi_vals = meas_in['azi'][inds]
    inc_vals = meas_in['inc'][inds]

    # Inc vs azi
    f, ax = plt.subplots()
    ax.scatter(azi_vals, inc_vals)

    ax.set_ylabel("Incidence angle (degree)")
    ax.set_xlabel("Azimuth angle (degree)")
    ax.set_xlim((-180, 180))
    ax.set_xticks(45 * np.arange(9) - 180)
    ax.set_title("AMSR-E")
    ax.grid()

    basename = "amsr_inc_" + \
               "x_{}_{}_".format(args.col, args.dx) + \
               "y_{}_{}".format(args.row, args.dy)
    f.savefig(basename + ".png", bbox_inches='tight')
    f.savefig(basename + ".pdf", bbox_inches='tight')
    # plt.close(f)

    # Tb vs azi
    out_info = {}
    for freq in ('6', '10', '18', '23', '36'):
        tb_v_vals = meas_in['tb_' + freq + 'v'][inds]
        tb_h_vals = meas_in['tb_' + freq + 'h'][inds]

        tb_v_mean = tb_v_vals.mean()
        tb_h_mean = tb_h_vals.mean()

        tb_v_vals -= tb_v_mean
        tb_h_vals -= tb_h_mean

        f_fit_v = compute_fourier_fit(azi_vals, tb_v_vals)
        f_fit_h = compute_fourier_fit(azi_vals, tb_h_vals)

        f, ax = plt.subplots(2, 1, sharex=True)
        ax[0].scatter(azi_vals, tb_v_vals)
        ax[1].scatter(azi_vals, tb_h_vals)
        ax[0].axhline(ls=':')
        ax[1].axhline(ls=':')

        f_fit_azi = np.linspace(-180, 180, 100)
        f_fit_tb_v = apply_fourier_fit(f_fit_azi, f_fit_v)
        f_fit_tb_h = apply_fourier_fit(f_fit_azi, f_fit_h)
        ax[0].plot(f_fit_azi, f_fit_tb_v)
        ax[1].plot(f_fit_azi, f_fit_tb_h)

        ax[0].set_ylabel("Residual $T_b$ v-pol (K)")
        ax[1].set_ylabel("Residual $T_b$ h-pol (K)")
        ax[1].set_xlabel("Azimuth angle (degree)")
        ax[1].set_xlim((-180, 180))
        ax[1].set_xticks(45 * np.arange(9) - 180)
        ax[0].set_title("AMSR-E {} GHz".format(freq))

        basename = "amsr_tb_{}_".format(freq) + \
                   "x_{}_{}_".format(args.col, args.dx) + \
                   "y_{}_{}".format(args.row, args.dy)
        f.savefig(basename + ".png", bbox_inches='tight')
        f.savefig(basename + ".pdf", bbox_inches='tight')
        # plt.close(f)

        # Store results
        out_info['tb_' + freq + 'v'] = {
            'N': tb_v_vals.size,
            'mean': tb_v_mean.item(),
        }
        out_info['tb_' + freq + 'h'] = {
            'N': tb_h_vals.size,
            'mean': tb_h_mean.item(),
        }
        out_info['tb_' + freq + 'v'].update(f_fit_v)
        out_info['tb_' + freq + 'h'].update(f_fit_h)

    out_info['lat'] = bin_lat
    out_info['lon'] = bin_lon
    out_info['x'] = bin_x
    out_info['y'] = bin_y

    # Store output info
    basename = "amsr_".format(freq) + \
               "x_{}_{}_".format(args.col, args.dx) + \
               "y_{}_{}".format(args.row, args.dy)
    print("Storing info to {}".format(basename + ".json"))
    with open(basename + ".json", "wt") as f:
        json.dump(out_info, f, indent=2, sort_keys=True)

    return


def apply_fourier_fit(azi: np.ndarray,
                      fit_info: typing.Mapping) -> np.ndarray:
    """Apply the second-order Fourier series fit.

    azi: azimuth angles (in degrees), probably np.linspace(-180, 180, 100)
    fit_info: dict with the fit coefficients

    """
    azi_rad = np.deg2rad(azi)
    return fit_info['B0'] + \
        fit_info['B1'] * np.cos(azi_rad) + \
        fit_info['C1'] * np.sin(azi_rad) + \
        fit_info['B2'] * np.cos(2 * azi_rad) + \
        fit_info['C2'] * np.sin(2 * azi_rad)


def compute_fourier_fit(azi_vals: np.ndarray,
                        tb_vals: np.ndarray) -> typing.Dict:
    """Fit a 2nd order Fourier series to the Tb/azi data.

    azi_vals and tb_vals are 1D numpy arrays of the same shape (in
    units of degrees and kelvin).

    A second-order Fourier series is fit to the data in a
    least-squares sense and the coefficients of the fit are returned.

    These coefficients are: (D0, D1, D2, theta1, theta2) for the
    magnitude and phase of the terms. Also, the rectangular
    coordinates are returned, (B0, B1, C1, B2, C2).

    TODO: I may want to precompute the matrix A and re-apply it for
    each Tb channel rather than re-computing it for each case.

    """
    # Check inputs
    if azi_vals.shape != tb_vals.shape:
        raise Exception("Shapes don't match")

    # Construct the matrix A
    azi_rad = np.deg2rad(azi_vals)
    N = azi_rad.size
    M = 5
    A = np.empty((N, M))
    A[:, 0] = 1
    A[:, 1] = np.cos(azi_rad)
    A[:, 2] = np.sin(azi_rad)
    A[:, 3] = np.cos(2 * azi_rad)
    A[:, 4] = np.sin(2 * azi_rad)

    # Solve for the coefficients
    soln = np.linalg.lstsq(A, tb_vals)
    xhat = soln[0]

    results = {
        # Rectangular coordinates
        'B0': xhat[0],
        'B1': xhat[1],
        'C1': xhat[2],
        'B2': xhat[3],
        'C2': xhat[4],
        # Polar coordinates
        'D0': xhat[0],
        'D1': np.hypot(xhat[1], xhat[2]),
        'D2': np.hypot(xhat[3], xhat[4]),
        'theta1': np.arctan2(xhat[2], xhat[1]),
        'theta2': np.arctan2(xhat[4], xhat[3]),
        # Matrix info
        'A_rank': np.linalg.matrix_rank(A),
        'A_cond': np.linalg.cond(A),
    }

    # Convert numpy scalars to Python types
    return {key: val.tolist() for key, val in results.items()}


def display_proc_time(start_time: float) -> None:
    """Display how long the processing took.

    start_time: when time.perf_counter() was called at the start of processing

    """
    dt = time.perf_counter() - start_time
    print("Finished processing in {:0.2f} s".format(dt))


if __name__ == "__main__":
    main()
    plt.show()
