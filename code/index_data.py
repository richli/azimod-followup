#!/usr/bin/env python3
"""Index the collected AMSR-E data.

The region data file is already annotated with what grid cell each
measurement belongs to. This stores the grid cell information to a
SQLite database and creates indices over the row/col columns. This
enables much faster lookups for later processing.

"""
# Version history:
# v0.1 2016-07-16:
#    Created

import argparse
import time
import atexit
import typing

from netCDF4 import Dataset
import apsw

__author__ = "Richard Lindsley"
__version__ = "0.1"


def main() -> None:
    """Main function."""
    # ----------------
    # Parse arguments
    # ----------------
    parser = argparse.ArgumentParser(
        description='Index collected AMSR-E data')
    parser.add_argument('in_fname', action='store',
                        help='AMSR-E data in the Antarctic region (netCDF)')
    parser.add_argument('out_fname', action='store',
                        help='Output file containing grid index (SQLite)')

    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(__version__))
    args = parser.parse_args()

    # Register exit function
    atexit.register(display_proc_time, time.perf_counter())

    # -----------------
    # Open input and output files
    # -----------------
    print("Creating output file: {}".format(args.out_fname))
    conn = apsw.Connection(args.out_fname)
    print("Opening input file: {}".format(args.in_fname))
    with Dataset(args.in_fname, "r") as in_f:
        conn.cursor().execute("PRAGMA journal_mode=WAL")
        # conn.cursor().execute("PRAGMA synchronous=off")
        # -----------------
        # Perform the indexing
        # -----------------
        process_file(in_f, conn)

    conn.close()

    return


def process_file(in_f: Dataset,
                 conn: apsw.Connection) -> None:
    """Read the region data file and write out the index.

    in_f: the input netCDF Dataset object
    conn: the output SQLite connection

    """
    print("  reading data")
    bin_rows = in_f['bin_row'][:].tolist()
    bin_cols = in_f['bin_col'][:].tolist()
    records = range(len(bin_rows))

    print("  preparing output")
    with conn:
        c = conn.cursor()
        c.execute("""
        CREATE TABLE grid(id INTEGER PRIMARY KEY, row INTEGER, col INTEGER)
        """)

    print("  writing output")
    last_display = time.perf_counter()
    with conn:
        c = conn.cursor()
        for i in range(len(records)):
            if (i + 1) % 100000 == 0:
                now = time.perf_counter()
                if now - last_display > 2:
                    print("  {}/{} ({:0.1%})".format(i + 1, len(records),
                                                     (i + 1) / len(records)))
                    last_display = now

            c.execute("INSERT INTO grid VALUES(?, ?, ?)",
                      (records[i], bin_rows[i], bin_cols[i]))

    print("  creating indices")
    with conn:
        c = conn.cursor()
        c.execute("CREATE INDEX grid_loc ON grid(row, col)")


def display_proc_time(start_time) -> None:
    """Display how long the processing took.

    start_time: when time.perf_counter() was called at the start of processing

    """
    dt = time.perf_counter() - start_time
    print("Finished indexing in {:0.2f} s".format(dt))


if __name__ == "__main__":
    main()
