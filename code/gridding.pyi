# Python type hints for gridding.pyx
import typing

import numpy as np

def process_data(meas_in: typing.Mapping[str, np.ndarray],
                 grid_out: typing.Mapping[str, np.ndarray]): ...
def extract_data(meas_in: typing.Mapping[str, np.ndarray],
                 col: int, row: int) -> typing.List[int]: ...
