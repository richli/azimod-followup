#!/usr/bin/env python3
"""Grid the collected AMSR-E data.

The region data file is already annotated with what grid cell each
measurement belongs to. This goes through each valid grid cell and
sets the grid cell value to be the average of all values that belong
to the grid cell. (A drop-in-the-bucket gridding technique.)

The output is saved to a netCDF file. The South Hemisphere EASE-2 grid
is used, with a grid spacing of 12.5 km.

"""
# Version history:
# v0.1 2016-07-16:
#    Created
# v0.2 2016-07-20:
#    Finish gridding
# v0.3 2016-07-21:
#    Implement gridding in Cython
# v0.4 2016-10-05:
#    Include Fourier series fit

import argparse
import sys
from pathlib import Path
import time
from datetime import datetime
import subprocess
import atexit
import typing

import numpy as np
from numpy import ma
import netCDF4
from netCDF4 import Dataset, default_fillvals
from dateutil import tz
import pyproj

from gridding import process_data

__author__ = "Richard Lindsley"
__version__ = "0.4"


def main() -> None:
    """Main function."""
    # ----------------
    # Parse arguments
    # ----------------
    parser = argparse.ArgumentParser(
        description='Grid collected AMSR-E data')
    parser.add_argument('in_fname', action='store',
                        help='AMSR-E data in the Antarctic region (netCDF)')
    parser.add_argument('out_fname', action='store',
                        help='Output file containing results (netCDF)')

    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(__version__))
    args = parser.parse_args()

    # Register exit function
    atexit.register(display_proc_time, time.perf_counter())

    # -----------------
    # Load input, prepare output arrays
    # -----------------
    print("Opening input file: {}".format(args.in_fname))
    with Dataset(args.in_fname, "r") as in_f:
        print("  loading data")
        meas_in = {
            'num_recs': len(in_f.dimensions['record']),
            'bin_rows': in_f.variables['bin_row'][:],
            'bin_cols': in_f.variables['bin_col'][:],
            'tb_6h': in_f.variables['tb_6h'][:],
            'tb_6v': in_f.variables['tb_6v'][:],
            'tb_10h': in_f.variables['tb_10h'][:],
            'tb_10v': in_f.variables['tb_10v'][:],
            'tb_18h': in_f.variables['tb_18h'][:],
            'tb_18v': in_f.variables['tb_18v'][:],
            'tb_23h': in_f.variables['tb_23h'][:],
            'tb_23v': in_f.variables['tb_23v'][:],
            'tb_36h': in_f.variables['tb_36h'][:],
            'tb_36v': in_f.variables['tb_36v'][:],
            'azi': in_f.variables['azi'][:],
        }

    # Concatenate these variables for easier use later
    num_channels = 10
    meas_in['tb'] = np.empty((meas_in['num_recs'], num_channels),
                             dtype=np.float32)
    meas_in['tb'][:, 0] = meas_in['tb_6h']
    meas_in['tb'][:, 1] = meas_in['tb_6v']
    meas_in['tb'][:, 2] = meas_in['tb_10h']
    meas_in['tb'][:, 3] = meas_in['tb_10v']
    meas_in['tb'][:, 4] = meas_in['tb_18h']
    meas_in['tb'][:, 5] = meas_in['tb_18v']
    meas_in['tb'][:, 6] = meas_in['tb_23h']
    meas_in['tb'][:, 7] = meas_in['tb_23v']
    meas_in['tb'][:, 8] = meas_in['tb_36h']
    meas_in['tb'][:, 9] = meas_in['tb_36v']

    # Free some memory
    for key in {'tb_6v', 'tb_6h', 'tb_10v', 'tb_10h', 'tb_18v', 'tb_18h',
                'tb_23v', 'tb_23h', 'tb_36v', 'tb_36h'}:
        del meas_in[key]

    # The arrays are indexed as (x, y) in the map projection
    # coordinates, or (col, row) in the input variables. Some
    # variables are additionally indexed by channel (6h, 6v, ...).
    # Note that the 3D arrays are arranged in C (row-major) order to
    # take advantage of the typical access pattern. But when they're
    # written out to netCDF, they're transposed to a different order.
    num_rows, num_cols = 1440, 1440
    grid_dims_3d = (num_cols, num_rows, num_channels)
    grid_dims_2d = (num_cols, num_rows)
    grid_out = {
        'num_hits': np.zeros(grid_dims_2d, dtype=np.int16),
        'matrix_cond': np.zeros(grid_dims_2d, dtype=np.float64),
        'mean': np.zeros(grid_dims_3d, dtype=np.float64),
        'mag_0': np.zeros(grid_dims_3d, dtype=np.float64),
        'mag_1': np.zeros(grid_dims_3d, dtype=np.float64),
        'mag_2': np.zeros(grid_dims_3d, dtype=np.float64),
        'ang_1': np.zeros(grid_dims_3d, dtype=np.float64),
        'ang_2': np.zeros(grid_dims_3d, dtype=np.float64),
        'mean_err_mean': np.zeros(grid_dims_3d, dtype=np.float64),
        'mean_err_std': np.zeros(grid_dims_3d, dtype=np.float64),
        'azi_err_mean': np.zeros(grid_dims_3d, dtype=np.float64),
        'azi_err_std': np.zeros(grid_dims_3d, dtype=np.float64),
        # 'azi_m1': np.zeros(grid_dims_2d, dtype=np.complex128),
        'azi_m1_r': np.zeros(grid_dims_2d, dtype=np.float64),
        'azi_m1_i': np.zeros(grid_dims_2d, dtype=np.float64),
    }

    # -----------------
    # Write header to output file
    # -----------------
    print("Opening output file: {}".format(args.out_fname))
    with Dataset(args.out_fname, "w") as out_f:
        print("  writing header")
        write_header(out_f)

        # -----------------
        # Perform the gridding
        # -----------------
        print("  processing grid")
        process_data(meas_in, grid_out)

        # -----------------
        # Write output
        # -----------------
        chan_dict = out_f['chan'].datatype.enum_dict
        mask = grid_out['num_hits'] == 0
        chan_mask = np.broadcast_to(mask, (len(chan_dict),
                                           mask.shape[0], mask.shape[1]))

        out_f['num_hits'][:] = ma.masked_equal(grid_out['num_hits'], 0)
        out_f['matrix_cond'][:] = ma.array(grid_out['matrix_cond'], mask=mask)

        # Note that the channel dimension is handled okay (0 maps to
        # 6h, etc). But I need to re-order the arrays from (col, row,
        # channel) to (channel, col, row).
        for var in ('mean',
                    'mean_err_mean', 'mean_err_std',
                    'azi_err_mean', 'azi_err_std',
                    'mag_0', 'mag_1', 'mag_2',
                    'ang_1', 'ang_2'):
            grid_val = grid_out[var].astype(np.float32)
            grid_val = np.moveaxis(grid_val, 2, 0)
            out_f[var][:, :, :] = ma.array(grid_val, mask=chan_mask)

        # https://en.wikipedia.org/wiki/Directional_statistics
        azi_m1 = grid_out['azi_m1_r'] + 1j * grid_out['azi_m1_i']
        azi_mean = np.angle(azi_m1, deg=True)
        out_f['azi_mean'][:] = ma.array(azi_mean, mask=mask)
        azi_abs = ma.array(np.absolute(azi_m1), mask=mask)
        out_f['azi_std'][:] = ma.sqrt(-2 * ma.log(azi_abs))

    return


def write_header(out_f: Dataset) -> None:
    """Create and prepare the output file."""
    out_f.title = "Gridded AMSR-E data over Antarctica"
    out_f.Conventions = "CF-1.6,ACDD-1.3"
    out_f.creator_name = "Richard Lindsley"
    out_f.creator_type = "person"
    out_f.netcdf_version_id = netCDF4.getlibversion().split()[0]
    timestamp = datetime.strftime(datetime.now(tz.tzlocal()),
                                  "%Y-%m-%d %H:%M:%S%z")
    arg_list = [Path(__file__).name, *sys.argv[1:]]
    hist_entry = ("{} created by {} (commit {}) "
                  "as: {}").format(timestamp, arg_list[0],
                                   get_git_rev(),
                                   " ".join(arg_list))
    out_f.date_created = timestamp
    out_f.history = hist_entry

    out_f.region = 'antarctica'
    out_f.geospatial_lat_min = np.float32(-90)
    out_f.geospatial_lat_max = np.float32(-45)
    out_f.geospatial_lon_min = np.float32(-180)
    out_f.geospatial_lon_max = np.float32(180)

    # Create dimensions and variables
    num_rows = num_cols = 1440
    num_channels = 10
    out_f.createDimension("y", num_rows)
    out_f.createDimension("x", num_cols)
    out_f.createDimension("chan", num_channels)
    out_f.createVariable("y", np.float32, ("y", ))
    out_f.createVariable("x", np.float32, ("x", ))

    chan_dict = {'6h': 1, '6v': 2, '10h': 3, '10v': 4,
                 '18h': 5, '18v': 6, '23h': 7, '23v': 8,
                 '36h': 9, '36v': 10}
    chan_type = out_f.createEnumType(np.uint8, 'chan_t', chan_dict)

    out_f.createVariable("chan", chan_type, ("chan", ))

    out_f.createVariable("ease2s", np.int32, ())

    filters = {'fletcher32': True, 'zlib': True}
    i16_fill = default_fillvals['i2']
    f32_fill = default_fillvals['f4']
    vars_2d_f32 = ["azi_mean", "azi_std", "lat", "lon",
                   "matrix_cond", ]
    vars_2d_i16 = ["num_hits", ]
    vars_3d_f32 = ["mean", "mag_0", "mag_1", "mag_2",
                   "ang_1", "ang_2", "mean_err_mean", "mean_err_std",
                   "azi_err_mean", "azi_err_std"]
    for v in vars_2d_f32:
        out_f.createVariable(v, np.float32, ("x", "y"),
                             fill_value=f32_fill, **filters)
    for v in vars_2d_i16:
        out_f.createVariable(v, np.int16, ("x", "y"),
                             fill_value=i16_fill, **filters)
    for v in vars_3d_f32:
        out_f.createVariable(v, np.float32, ("chan", "x", "y"),
                             fill_value=f32_fill, **filters)

    # Variable attributes
    out_f['lat'].setncatts({
        'standard_name': 'latitude',
        'units': 'degrees_north',
        'valid_range': np.array([-90, 90], dtype=np.float32),
    })
    out_f['lon'].setncatts({
        'standard_name': 'longitude',
        'units': 'degrees_east',
        'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['chan'].setncatts({
        'comment': ('AMSR-E brightness temperature at a '
                    'certain frequency and polarization'),
    })
    # freqs = {'tb_6v': '6.9',
    #          'tb_6h': '6.9',
    #          'tb_10v': '10.7',
    #          'tb_10h': '10.7',
    #          'tb_18v': '18.7',
    #          'tb_18h': '18.7',
    #          'tb_23v': '23.8',
    #          'tb_23h': '23.8',
    #          'tb_36v': '36.5',
    #          'tb_36h': '36.5'}
    # pols = {v: 'h' if v.endswith('h') else 'v' for v in tb_groups}
    # long_names = {v: 'AMSR-E Tb at {} GHz, {}-pol'.format(freqs[v], pols[v])
    #               for v in tb_groups}
    # for grp_name in tb_groups:
    #     grp = out_f.groups[grp_name]
    #     grp.setncattr('long_name', long_names[grp_name])

    out_f['ease2s'].setncatts({
        'long_name': 'EASE2_S12.5km',
        'grid_mapping_name': 'lambert_azimuthal_equal_area',
        'latitude_of_projection_origin': np.float32(-90),
        'longitude_of_projection_origin': np.float32(0),
        'false_easting': np.float32(0),
        'false_northing': np.float32(0),
        'scale_factor_at_projection_origin': np.float32(25e3),
        'proj4text': ('+proj=laea +lat_0=-90 +lon_0=0 +x_0=0 +y_0=0 '
                      '+ellps=WGS84 +datum=WGS84 +units=m'),
        'srid': 'urn:ogc:def:crs:EPSG::6932',
    })
    out_f['x'].setncatts({
        'long_name': 'x coordinate of projection',
        'standard_name': 'projection_x_coordinate',
        'units': 'm',
        'valid_range': np.array([-9e6, 9e6], dtype=np.float32),
    })
    out_f['y'].setncatts({
        'long_name': 'y coordinate of projection',
        'standard_name': 'projection_y_coordinate',
        'units': 'm',
        'valid_range': np.array([-9e6, 9e6], dtype=np.float32),
    })
    out_f['azi_mean'].setncatts({
        'long_name': 'mean azimuth angle',
        'units': 'degree',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })
    out_f['azi_std'].setncatts({
        'long_name': 'standard deviation of azimuth angle',
        'units': 'degree',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
    })

    out_f['mean'].setncatts({
        'long_name': 'mean brightness temperature',
        # 'valid_range': np.array([100, 400], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['mag_0'].setncatts({
        'long_name': 'magnitude of non-sinusoidal brightness temperature',
        # 'valid_range': np.array([0, 400], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['mag_1'].setncatts({
        'long_name': 'magnitude of first-harmonic brightness temperature',
        # 'valid_range': np.array([0, 400], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['mag_2'].setncatts({
        'long_name': 'magnitude of second-harmonic brightness temperature',
        # 'valid_range': np.array([0, 400], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['ang_1'].setncatts({
        'long_name': ('phase angle of first-harmonic '
                      'brightness temperature'),
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
        'units': 'degree',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['ang_2'].setncatts({
        'long_name': ('phase angle of second-harmonic '
                      'brightness temperature'),
        # 'valid_range': np.array([-180, 180], dtype=np.float32),
        'units': 'degree',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['mean_err_mean'].setncatts({
        'long_name': ('mean of residual error between '
                      'measured and modeled brightness temperature'),
        'comment': ('The modeled brightness temperature is simply '
                    'the mean brightness temperature'),
        # 'valid_range': np.array([-5, 5], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['mean_err_std'].setncatts({
        'long_name': ('standard deviation of residual error between '
                      'measured and modeled brightness temperature'),
        'comment': ('The modeled brightness temperature is simply '
                    'the mean brightness temperature'),
        # 'valid_range': np.array([0, 5], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['azi_err_mean'].setncatts({
        'long_name': ('mean of residual error between '
                      'measured and modeled brightness temperature'),
        'comment': ('The modeled brightness temperature is '
                    'from the azimuth modulation fit'),
        # 'valid_range': np.array([-5, 5], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['azi_err_std'].setncatts({
        'long_name': ('standard deviation of residual error between '
                      'measured and modeled brightness temperature'),
        'comment': ('The modeled brightness temperature is '
                    'from the azimuth modulation fit'),
        # 'valid_range': np.array([0, 5], dtype=np.float32),
        'units': 'K',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })

    out_f['num_hits'].setncatts({
        'long_name': 'number of observations',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })
    out_f['matrix_cond'].setncatts({
        'long_name': 'matrix condition number',
        'grid_mapping': 'ease2s',
        'coordinates': 'lat lon',
    })

    # Fill in coordinate variables. Note that x/y are the values at
    # the *center* of the grid cell.
    x_vals = (np.arange(num_cols) - num_cols // 2) * 12500.0 + (12500.0 / 2)
    y_vals = (np.arange(num_cols) - num_cols // 2) * 12500.0 + (12500.0 / 2)
    out_f['x'][:] = x_vals
    out_f['y'][:] = y_vals
    out_f['chan'][:] = list(sorted(chan_dict.values()))

    # Fill in lat/lon
    proj_ease = pyproj.Proj(proj='laea', lat_0=-90, lon_0=0, x_0=0, y_0=0,
                            ellps='WGS84', datum='WGS84', units='m')

    x_arr, y_arr = np.meshgrid(x_vals, y_vals, indexing='ij')
    lon_arr, lat_arr = proj_ease(x_arr, y_arr, inverse=True, errcheck=True)
    out_f['lat'][:] = lat_arr
    out_f['lon'][:] = lon_arr


def get_git_rev() -> str:
    """Return the git commit short hash for the CWD."""
    r = subprocess.run(['git', 'rev-parse', '--short', 'HEAD'],
                       stdout=subprocess.PIPE, check=True)

    result = typing.cast(bytes, r.stdout)
    return result.rstrip().decode()


def display_proc_time(start_time: float) -> None:
    """Display how long the processing took.

    start_time: when time.perf_counter() was called at the start of processing

    """
    dt = time.perf_counter() - start_time
    print("Finished processing in {:0.2f} s".format(dt))


if __name__ == "__main__":
    main()
