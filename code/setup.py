#!/usr/bin/env python

# python setup.py build_ext --inplace

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules = [
    Extension(
        'gridding', ['gridding.pyx'],
        libraries=['lapack'],
        extra_compile_args=['-fopenmp', '-flto'],
        extra_link_args=['-fopenmp', '-flto'],)
]

setup(
  name = 'gridding helper',
  ext_modules = cythonize(ext_modules),
)
