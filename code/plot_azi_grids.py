#!/usr/bin/env python3
"""Plot maps of retrieved parameters.

This script requires GMT <http://gmt.soest.hawaii.edu/> to be installed.

"""
# Version history:
# v0.1 2016-10-22:
#    Created

import argparse
import subprocess
from pathlib import Path
import concurrent.futures
import tempfile
from itertools import product
import typing

import numpy as np
from netCDF4 import Dataset, default_fillvals

__author__ = "Richard Lindsley"
__version__ = "0.1"

GridData = typing.NewType("GridData", typing.Dict[str, np.ndarray])


def main() -> None:
    """Main function."""
    # ----------------
    # Parse arguments
    # ----------------
    parser = argparse.ArgumentParser(
        description='Plot grid outputs on maps')
    parser.add_argument('in_fname', action='store',
                        help=("NetCDF file with retrieved parameters "
                              "(e.g., 'amsr_Ant_grid.nc'"))

    parser.add_argument('--out-dir', action='store',
                        help='Output directory to place plots, (e.g., ./)')
    parser.add_argument('--plt-fmt', action='store',
                        choices=['pdf', 'png'], default='png',
                        help='File format to save as (default: %(default)s)')
    parser.add_argument('--single', action='store_true',
                        help="Don't use multiple processes")
    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(__version__))
    args = parser.parse_args()

    # -----------------
    # Set up some plotting parameters
    # -----------------
    # All frequencies, polarizations, and channels. The order of chans
    # is important because it matches the ordering of the input file
    # arrays.
    freqs = ('6', '10', '18', '23', '36')
    # pols = ('h', 'v')
    chans = ('6h', '6v', '10h', '10v', '18h', '18v',
             '23h', '23v', '36h', '36v')
    # All kinds of data to map. Currently these all are also functions
    # of channel.
    kinds = ('mean', 'mag1', 'mag2', 'ang1', 'ang2')

    # Colormaps and such that vary as a function of AMSR-E channel
    cbar_ranges = {}
    cbar_ranges.update({'mean_' + freq + 'h': (100, 240) for freq in freqs})
    cbar_ranges.update({'mean_' + freq + 'v': (150, 260) for freq in freqs})
    cbar_ranges.update({'mag1_' + chan: (0, 4) for chan in chans})
    cbar_ranges.update({'mag2_' + chan: (0, 4) for chan in chans})
    cbar_ranges.update({'ang1_' + chan: (-180, 0, 180) for chan in chans})
    cbar_ranges.update({'ang2_' + chan: (-180, 0, 180) for chan in chans})

    cbar_maps = {'mean_' + chan: 'gray' for chan in chans}
    cbar_maps.update({'mag1_' + chan: 'gray' for chan in chans})
    cbar_maps.update({'mag2_' + chan: 'gray' for chan in chans})
    cbar_maps.update({'ang1_' + chan: 'black,white,black' for chan in chans})
    cbar_maps.update({'ang2_' + chan: 'black,white,black' for chan in chans})

    cbar_labels = {'mean_' + chan: 'K' for chan in chans}
    cbar_labels.update({'mag1_' + chan: 'K' for chan in chans})
    cbar_labels.update({'mag2_' + chan: 'K' for chan in chans})
    cbar_labels.update({'ang1_' + chan: 'degree' for chan in chans})
    cbar_labels.update({'ang2_' + chan: 'degree' for chan in chans})

    plot_titles = {}
    for chan in chans:
        plot_titles['mean_' + chan] = 'Mean TB at ' + chan
        plot_titles['mag1_' + chan] = ('Magnitude of first-harmonic '
                                       'TB at ' + chan)
        plot_titles['mag2_' + chan] = ('Magnitude of second-harmonic '
                                       'TB at ' + chan)
        plot_titles['ang1_' + chan] = ('Phase of first-harmonic '
                                       'TB at ' + chan)
        plot_titles['ang2_' + chan] = ('Phase of second-harmonic '
                                       'TB at ' + chan)

    # Map projection: Lambert azimuthal equal-area, 8cm wide; centered
    # at South pole
    proj = "-JA0/-90/8c"

    # This region bounds the South pole:
    # (ll=(-45 N, -135 E), ur=(-45 N, 45 E))
    reg = "-R-135/-45/45/-45r"

    plt_opts = {
        'out_dir': args.out_dir,
        'fmt': args.plt_fmt,
        'proj': proj,
        'reg': reg,
        'plot_titles': plot_titles,
        'cbar_labels': cbar_labels,
        'cbar_maps': cbar_maps,
        'cbar_ranges': cbar_ranges,
        'chans': chans,
    }

    # -----------------
    # Load data and plot it
    # -----------------
    print("Input file: {}".format(args.in_fname))

    # In both cases below load_data() is called multiple times for the
    # same inputs. Is this inefficient? Yes. But is this a bottleneck?
    # No; it's pretty quick and is far faster than the plot_map()
    # function that follows it.

    # Single-process mode
    if args.single:
        for kind, chan in product(kinds, chans):
            print(kind, chan)
            grid_data = load_data(kind, args.in_fname)
            plot_map(kind, chan, grid_data, plt_opts)

    # Process pool. Note that each process will print to stdout, which
    # isn't really that safe...but it's okay enough for now.
    else:
        fs = []
        with concurrent.futures.ProcessPoolExecutor() as e:
            for kind, chan in product(kinds, chans):
                grid_data = load_data(kind, args.in_fname)
                fs.append(e.submit(plot_map, kind, chan, grid_data, plt_opts))

            for fut in concurrent.futures.as_completed(fs):
                print("{}: done".format(fut.result()))

    return


def plot_map(kind: str,
             chan: str,
             grid_data: GridData,
             plt_opts: typing.Mapping[str, typing.Any]) -> str:
    """Use GMT to plot a single map."""
    kchan = '{}_{}'.format(kind, chan)
    # Prepare temporary directory for GMT (here we're running GMT in
    # "isolation mode", see
    # http://gmt.soest.hawaii.edu/doc/5.3.1/GMT_Docs.html#running-gmt-in-isolation-mode)
    with tempfile.TemporaryDirectory() as tmpdir:
        # print("  GMT working directory: {}".format(tmpdir))

        # GMT is super picky about its input data, so modify the data
        # a bit and rewrite it to netCDF for GMT to read. Besides the
        # coordinate variables, only a single variable is written to
        # the file: the kind/chan to plot.
        nc_fname = Path(tmpdir) / "map_data.nc"
        with Dataset(nc_fname, "w") as nc_f:
            nc_f.title = "Map data for {}, {}".format(kind, chan)
            nc_f.Conventions = "CF-1.6"

            # gridline registration
            nc_f.node_offset = np.array(0, dtype=np.int32)

            # Create dimensions and variables
            f32_fill = default_fillvals['f4']
            # i32_fill = default_fillvals['i4']
            nc_f.createDimension("x", len(grid_data['x']))
            nc_f.createDimension("y", len(grid_data['y']))

            nc_f.createVariable("x", np.int32, ("x", ))
            nc_f.createVariable("y", np.int32, ("y", ))
            nc_f.createVariable(kchan, np.float32, ("x", "y"),
                                fill_value=f32_fill)

            nc_f['/x'].units = "m"
            nc_f['/y'].units = "m"

            # Store values, but transpose everything (for grdproject below)
            nc_f['/x'][:] = grid_data['y']
            nc_f['/y'][:] = grid_data['x']
            ichan = plt_opts['chans'].index(chan)
            out_var = nc_f.variables[kchan]
            out_var[:, :] = grid_data[kind][ichan, :, :].T

        # Set common GMT options
        gmt_opts = {
            'GMT_COMPATIBILITY': '5',
            'PS_CHAR_ENCODING': 'Standard+',
            'FORMAT_GEO_MAP': 'DF',
            'FONT_ANNOT_PRIMARY': '10p',
            'FONT_LABEL': '10p',
            'FONT_TITLE': '12p',
            'MAP_TITLE_OFFSET': '-2p',
            'MAP_GRID_PEN_PRIMARY': 'thinnest,darkgray,-',
        }
        cmd = ['gmtset']
        for key, value in gmt_opts.items():
            cmd.extend([key, '=', value])
        call_gmt(cmd, tmpdir, prefix=kchan)

        # Open output PS file for GMT
        ps_file = Path(tmpdir) / "amsr_Ant_{}_{}.ps".format(kind, chan)
        ps_f = ps_file.open("wb")

        # Transform the input data from map coordinates to geographic
        # coordinates (since that's the only way GMT likes it)
        nc_reproj = Path(tmpdir) / "geo_data_{}.nc".format(kind)
        cmd = ['grdproject', plt_opts['proj'], plt_opts['reg'],
               '{}?{}'.format(nc_fname, kchan),
               '-G{}'.format(nc_reproj),
               '-I', '-Fe', '-C']
        call_gmt(cmd, tmpdir, prefix=kchan)

        # Define colormaps
        cpt_file = Path(tmpdir) / "{}.cpt".format(kchan)
        base_cpt = plt_opts['cbar_maps'][kchan]

        with cpt_file.open("wb") as cpt_f:
            cbar_range = plt_opts['cbar_ranges'][kchan]
            cmd = ['makecpt',
                   '-C{}'.format(base_cpt),
                   '-T{}/{}'.format(cbar_range[0], cbar_range[1]),
                   '-D', '-M', '--COLOR_NAN=lightgray',
                   '-Z']
            if kind.startswith('ang'):
                cmd.append('-T{},{},{}'.format(*cbar_range))
            else:
                cmd.append('-T{}/{}'.format(*cbar_range))
            call_gmt(cmd, tmpdir, prefix=kchan, stdout=cpt_f)

        # Draw map border and title
        cmd = ['psbasemap', plt_opts['proj'], plt_opts['reg'],
               '-BWSne+t{}'.format(plt_opts['plot_titles'][kchan]), '-Bag30',
               '-K']
        call_gmt(cmd, tmpdir, prefix=kchan, stdout=ps_f)

        # Draw map data
        cmd = ['grdimage', plt_opts['proj'], plt_opts['reg'],
               str(nc_reproj),
               '-C{}'.format(str(cpt_file)),
               '-O', '-K']
        call_gmt(cmd, tmpdir, prefix=kchan, stdout=ps_f)

        # Mask out non-land (ocean and sea ice) areas
        cmd = ['pscoast', plt_opts['proj'], plt_opts['reg'],
               '-A1000/0/1', '-Da', '-Swhite',
               '-O', '-K']
        call_gmt(cmd, tmpdir, prefix=kchan, stdout=ps_f)

        # Draw colorbar
        ext_str = ''
        # ext_opt = 'none'
        # if ext_opt == 'none':
        #     ext_str = ''
        # elif ext_opt == 'both':
        #     ext_str = '+e'
        # elif ext_opt == 'max':
        #     ext_str = '+ef'
        cmd = ['psscale', plt_opts['proj'], plt_opts['reg'],
               '-DJRM+w8c/0.5c+o0.4c/0{}'.format(ext_str),
               '-C{}'.format(str(cpt_file)),
               '-Baf+l{}'.format(plt_opts['cbar_labels'][kchan]),
               '--MAP_LABEL_OFFSET=8p',
               '-O', '-K']
        call_gmt(cmd, tmpdir, prefix=kchan, stdout=ps_f)

        # Draw gridlines and map scale on top
        cmd = ['psbasemap', plt_opts['proj'], plt_opts['reg'],
               '-Byg30', '-Bxg60',
               '-Ln0.15/0.1+c-70+w1000k+l+f',
               '--MAP_LABEL_OFFSET=4p',
               '-O']
        call_gmt(cmd, tmpdir, prefix=kchan, stdout=ps_f)

        # Close the PS file
        ps_f.close()

        # Convert to png or pdf
        if plt_opts['out_dir'] is not None:
            out_dir = plt_opts['out_dir']
        else:
            out_dir = Path.cwd()
        cmd = ['psconvert', str(ps_file),
               '-A', '-D{}'.format(out_dir), '-P']
        if plt_opts['fmt'] == 'png':
            cmd.append('-Tg')
            cmd.append('-E300')
        elif plt_opts['fmt'] == 'pdf':
            cmd.append('-Tf')
        else:
            raise NotImplementedError

        call_gmt(cmd, tmpdir, prefix=kchan)

        return kchan


def load_data(kind: str, fname: str) -> GridData:
    """Load a subset of data from the grid file.

    For the angle kinds ('ang1' and 'ang2'), the corresponding
    rectangular coordinates are also included.

    """
    with Dataset(fname, 'r') as in_f:
        grid_in = GridData({
            'x': in_f.variables['x'][:],
            'y': in_f.variables['y'][:],
        })
        if kind == 'mean':
            grid_in['mean'] = in_f.variables['mean'][:, :, :]
        elif kind == 'mag1':
            grid_in['mag1'] = in_f.variables['mag_1'][:, :, :]
        elif kind == 'mag2':
            grid_in['mag2'] = in_f.variables['mag_2'][:, :, :]
        elif kind == 'ang1':
            grid_in['ang1'] = in_f.variables['ang_1'][:, :, :]
        elif kind == 'ang2':
            grid_in['ang2'] = in_f.variables['ang_2'][:, :, :]

    return grid_in


def call_gmt(cmd: typing.Sequence[str],
             tmpdir: str,
             prefix: typing.Optional[str] = None,
             stdout: typing.Optional[typing.Any] = None) \
        -> subprocess.CompletedProcess:
    """Run GMT with the specified arguments.

    cmd: list of arguments to pass to gmt. "gmt" is prepended to cmd
    inside here.

    tmpdir: the working directory for GMT to run in

    stdout: optional file-like object to write stdout to

    """
    if prefix is not None:
        print("  {}: {}".format(prefix, cmd[0]))
    else:
        print("  {}".format(cmd[0]))

    gmt_cmd = ["gmt"]
    gmt_cmd += cmd
    return subprocess.run(gmt_cmd,
                          env={'GMT_TMPDIR': tmpdir},
                          stdout=stdout,
                          stderr=subprocess.PIPE,
                          check=True)


def get_git_rev() -> str:
    """Return the git commit short hash for the CWD."""
    r = subprocess.run(['git', 'rev-parse', '--short', 'HEAD'],
                       stdout=subprocess.PIPE, check=True,
                       cwd=str(Path(__file__).parent))

    result = typing.cast(bytes, r.stdout)
    return result.rstrip().decode()

if __name__ == "__main__":
    main()
