===================================================================
Azimuth Modulation of Radar Backscatter and Brightness Temperatures
===================================================================

This contains an analysis of the azimuth modulation in scatterometer
sigma0 data and radiometer brightness temperature over East
Antarctica.

Datasets
========

Currently, only AMSR-E data is used. The raw data from JAXA is
calibrated (and also resampled, but I don't use the resampled data) at
`RSS <http://www.remss.com/measurements/brightness-temperature>`_ and
the L2A data is distributed by the `NSIDC
<https://nsidc.org/data/AE_L2A>`_.

Downloading the AMSR-E data over HTTPS requires OAuth-2
authentication, which means it's a bit tricky to download with cURL or
similar. However, the data is also available via anonymous FTP, so
that's the easiest access method.

https://nsidc.org/data/docs/daac/ae_l2a_tbs/data.html

https://n5eil01u.ecs.nsidc.org/AMSA/AE_L2A.003/

ftp://n5eil01u.ecs.nsidc.org/SAN/AMSA/AE_L2A.003/

The files are organized into half-orbit granules and are
in HDF-EOS2 format. Note that the non-resampled Tb data is used, which
is essentially copied over from the L1A data (although RSS applies a
calibration to it).

In theory, if netCDF is compiled with HDF4 support, it can read the
HDF-EOS2 files. I tested it with the AMSR-2 files, and while that's
true, netcdf4-python can't really deal with the fact that there are
multiple variables ("latitude" and "longitude" for example) that have
the same name but differing dimensions. I think this variable
"overloading" is probably a violation of the netCDF data model so it's
little wonder it's hard to work with.

Instead I convert the HDF-EOS2 files to netCDF using `h4cflib
<http://hdfeos.org/software/h4cflib.php>`_, so now the converted
netCDF files no longer have variable name clashes.

Region Definition
=================

The region is defined as everything south of -50 degrees North. The
data is gridded using the drop-in-the-bucket method where all
measurements whose lat/lon falls within the same grid cell are
averaged together (or processed together, when it comes to estimating
the model fit per-pixel).

To match the previous work, the time period is set to 2009-07-30 to
2009-08-28. 

The grid is the NSIDC `EASE-2
<https://nsidc.org/data/ease/ease_grid2.html>`_ South Hemisphere grid,
with a grid spacing of 12.5 km. It uses the Lambert azimuthal
equal-area projection. Note that the PROJ.4 string provided in the
EASE-2 reference is wrong for the S. Hemisphere: the ``lat_0`` should
be -90, not 90.

PROJ.4 is used to convert the measurement latitude/longitudes to map
coordinates. The PROJ.4 conversion string is: ``+proj=laea +lat_0=-90
+lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m``

Processing Overview
===================

``driver.sh`` handles everything. It downloads the AMSR-E data,
collects the data for the Antarctic region, processes the data onto a
grid, and plots some pixel results.

Python scripts are used to handle all the data processing, with some
of the inner loops written in Cython.

TODO
====

- Update ``plot_azi_pixel.py`` for the Gaussian model; then update
  ``grid_data.py`` accordingly
